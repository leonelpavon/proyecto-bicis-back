const nodemailer = require("nodemailer"); 
const sgTransport= require("nodemailer-sendgrid-transport"); 

let mailConfig; 
if (process.env.NODE_ENV === "production") { 
    const options = { 
        auth: { 
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig= sgTransport(options); 

} else { 
    if (process.env.NODE_ENV === "staging") { 
        console.log("XXXXXXXXX"); 
        const options = { 
            auth: { 
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig= sgTransport(options); 

} else { 
    mailConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: process.env.ethereal_user,
            pass: process.env.ethereal_pwd
        }
    };

}
}

module.exports = nodemailer.createTransport(mailConfig); 

/*
else { 
    mailConfig = {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'jimmie.macgyver70@ethereal.email',
            pass: 'z5kst67SRQ818BXdNz'
        }
    };

}
*/

