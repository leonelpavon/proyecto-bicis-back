var mongoose= require("mongoose"); 
var Bicicleta = require ("../../../models/bicicleta"); 
var request = require ("request"); 
var server = require ("../../../bin/www");  

var base_url= "http://localhost:3000/api/bicicletas";

const aBici = {
    id: 10,
    color: "rojo",
    modelo: "urbana",
    latitud: -34,
    longitud: -54
  };


const headers = { 'content-type': 'application/json' };

describe ("Testing API", function () { 
    beforeAll((done) => { 
        mongoose.connection.close(done) })
        var mongoDb = "mongodb://localhost/testdb"; 
        mongoose.connect(mongoDb, {useNewUrlParser: true, useUnifiedTopology: true});
    
        const db = mongoose.connection; 
        db.on("error", console.error.bind(console, "connection error")); 
        db.once("open", function(){ 
         console.log("We are connected to test Database!"); 
      });
    }); 
    
    afterEach(function(done){ 
        Bicicleta.deleteMany({}, function(err, success){ 
            if (err) console.log (err);
            done() 
        }); 
    }); 


    describe ("Get bicicletas", () => { 
        it ("Status 200", (done) => { 
            request.get(base_url, function (error, response, body) { 
            var result= JSON.parse(body); 

            expect(response.statusCode).toBe(200); 
            expect(response.bicicletas.length).toBe(0); 
            done(); 
           
            }); 
        }); 
    });
    
    describe("POST BICICLETAS /create", () => { 
        it ("Status 200", (done) => { 
            
            request.post({ 
                headers, 
                url: `${base_url}/create`,
             }, 
                function(error, response, body) { 
                    
                expect (console.log('error:', error));
                expect('statusCode:', response && response.statusCode).toBe(200); 
                var bici= JSON.parse(body).bicicleta; 
                console.log(bici); 
               
                expect(bici.color).toBe(aBici.color); 
                expect(bici.ubicacion[0]).toBe(aBici.latitud);
                expect(bici.ubicacion[1]).toBe(aBici.longitud);
                done(); 
            });

        });
    });
    
    
    describe("Delete bicicletas /delete", () => { 
        it ("status 204", (done) => { 
            
        var a = Bicicleta.createInstance(1, "rojo", "urbana", [-34, -45]);
        Bicicleta.add(a, function(err, newBici) { 
            if (err) console.log (err); 

        var headers = {"content-type" : "application/json"};
        var aBici = {"code": 1, "color": "rojo", "modelo": "urbana", "lat": -34, "long": -45};
        request.delete({ 
        headers:headers, 
        url: base_url + "/delete", 
        body: aBici }, 
        function(error, response, body) { 
            
            expect(response.statusCode).toBe(204); 
            var bici= JSON.parse(body).bicicleta; 
            expect(bici.length).toBe(0); 
            done(); 

        });
       
   });
   }); 
   });