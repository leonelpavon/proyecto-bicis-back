var mongoose= require("mongoose"); 
var Bicicleta = require("../../../models/bicicleta");  

describe ("Testing Bicicletas", function() {  
    beforeAll((done) => { 
    mongoose.connection.close(done) })
    var mongoDb =  'mongodb://localhost/testdb';
    mongoose.connect(mongoDb, {useNewUrlParser: true, useUnifiedTopology: true}); 

   const db = mongoose.connection; 
   db.on("error", console.error.bind(console, "connection error")); 
   db.once("open", function(){ 
    console.log("We are connected to test Database!"); 
 }); 
}); 

    afterEach(function(done){ 
        Bicicleta.deleteMany({}, function(err, success){ 
            if (err) console.log (err);
            done() 
        }); 
    }); 


describe("Bicicleta.createInstance", () => { 
    it ("se crea instancia de bici", () => { 
        
    var bici = Bicicleta.createInstance(1, "rojo", "urbana", [-34.761247, -58.398318]); 
        
    expect(bici.code).toBe(1); 
    expect(bici.color).toBe("rojo"); 
    expect(bici.modelo).toBe("urbana"); 
    expect(bici.ubicacion[0]).toEqual(-34.761247); 
    expect(bici.ubicacion[1]).toEqual(-58.398318); 

    }); 
});


describe("Bicicleta.allBicis", () => { 
    it ("comienza vacía", (done) => { 
    Bicicleta.allBicis(function (err, bicis) { 

        expect(bicis.length).toBe(0); 
        done();
    });
});
}); 

describe("Bicicleta.add", () => { 
    it ("agrega una bicicleta", (done) => { 
        var aBici = new Bicicleta({code: 1, color: "rojo", modelo: "urbana"});
        Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log (err);
        Bicicleta.allBicis (function(err, bicis) { 
            expect(bicis.length).toEqual(1);
            expect(bicis[0].code).toEqual(aBici.code); 


            done(); 
        
    });

    }); 
    
    });
                    
    });

    describe ("Bicicleta.findByCode", () => { 
        it("Busca la bici code 1", (done) => { 
        Bicicleta.allBicis (function(err, bicis) { 
            expect(bicis.length).toBe(0);
        
        var aBici = new Bicicleta({code: 1, color: "rojo", modelo: "urbana"}); 
        Bicicleta.add(aBici, function(err, newBici) { 
            if (err) console.log (err);

            
        var aBici2 = new Bicicleta({code: 2, color: "amarillo", modelo: "carreras"});
        Bicicleta.add(aBici2, function(err, newBici) { 
            if (err) console.log (err); 
            
        Bicicleta.findByCode(1, function (error, targetBici) { 
                expect(targetBici.code).toBe(aBici.code); 
                expect(targetBici.color).toBe(aBici.color); 
                expect(targetBici.modelo).toBe(aBici.modelo);
                
                done();  
            });
            });
        });
        });

    });
 });


    describe("Bicicleta.removeByCode", () => { 
         it ("se borra bici", (done) => {
    Bicicleta.allBicis(function (err, bicis) { 
    expect(bicis.length).toBe(0); 
   
    var aBici = new Bicicleta({code: 1, color: "rojo", modelo: "urbana"}); 
    var aBici2 = new Bicicleta({code: 2, color: "amarillo", modelo: "carreras"});

    Bicicleta.add(aBici, function(err, newBici) { 
        if (err) console.log (err);
    Bicicleta.add(aBici2, function(err, newBici) { 
            if (err) console.log (err);

    Bicicleta.removeByCode(1, function (err, removeBici) { 
        if (err) console.log (err);

    Bicicleta.allBicis (function(err, bicis) { 
        expect(bicis.length).toEqual(1)
        done(); 
    });
    });
    });
});
}); 
}); 
    }); 
   
 
 
 
/* Metodos anteriores declarados pre-Mongoose

 describe("Bicicleta.add", () => { 
    it ("se crea bici", () => { 
    expect(Bicicleta.allBicis.length).toBe(0); 

    var a = new Bicicleta(1, "rojo", "urbana", [-34.761247, -58.398318]); 
    Bicicleta.add(a); 

    expect(Bicicleta.allBicis.length).toBe(1); 
    expect(Bicicleta.allBicis[0]).toBe(a); 
    });
});


describe("Bicicleta.findById", () => {
    it ("debe devolver bici con id 1", () => { 
    expect(Bicicleta.allBicis.length).toBe(0); 

    var aBici = new Bicicleta(1, "rojo", "urbana");
    var aBici2 = new Bicicleta(2, "amarillo", "carreras");

    Bicicleta.add(aBici); 
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1); 
    expect(targetBici.id).toBe(1); 
    expect(targetBici.color).toBe(aBici.color); 
    expect(targetBici.modelo).toBe(aBici.modelo); 

});
});

describe("Bicicleta.removeById", () => { 
    it ("se borra bici", () => { 
    
    expect(Bicicleta.allBicis.length).toBe(0); 

    var a = new Bicicleta(1, "rojo", "urbana");
    Bicicleta.add(a); 

    expect(Bicicleta.allBicis.length).toBe(1); 
    var targetBici= Bicicleta.removeById(1); 
    expect(Bicicleta.allBicis.length).toBe(0); 

    });
});

*/