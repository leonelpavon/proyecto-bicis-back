Red de Bicicletas es un proyecto creado para el curso de "Desarrollo del lado del servidor: Nodejs,ExpressJS,MongoDB"
otorgado por la Universidad Austral a través de la plataforma Coursera. 

En el mismo se desarrolló el soporte que toda aplicación necesita para lidiar con la persistencia de la información, el setup de un servidor web, la creación de una API REST, autenticación y autorización, y la integración de librerías de terceros
(Express, Mongoose, Mongo DB, Jasmine y Heroku, entre las más importantes). 

Para mas referencias pueden visitar: https://www.coursera.org/learn/desarrollo-lado-servidor-nodejs-express-mongodb

PD: Recuerde instalar todas las dependencias con npm install. 
Para ver el proyecto en heroku, puede visitar:  https://bicisred.herokuapp.com/
